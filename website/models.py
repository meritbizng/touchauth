from django.db import models


class Company(models.Model):
	name = models.CharField(max_length=50)
	email = models.EmailField()
	logo = models.ImageField(upload_to='logos')
	website = models.CharField(max_length=100)

	def __str__(self):
		return str(self.name)


class Employee(models.Model):
	first_name = models.CharField(max_length=50)
	last_name = models.CharField(max_length=50)
	company_name = models.ForeignKey(Company, on_delete=models.CASCADE)
	email = models.EmailField()
	phone = models.CharField(max_length=20)

	def __str__(self):
		return str(self.first_name) + ' ' +str(self.last_name) + ' - ' +str(self.company_name)
