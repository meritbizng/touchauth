from django import forms
from .models import *

class CompanyCreate(forms.ModelForm):
    class Meta:
        model = Company
        fields = '__all__'


class EmployeeCreate(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'
