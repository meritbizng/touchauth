# Generated by Django 2.2.7 on 2019-11-29 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=models.FileField(upload_to='logos'),
        ),
    ]
