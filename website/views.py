from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import *
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.contrib import messages


def index(request):
	companies = Company.objects.all()
	employees = Employee.objects.all()
	return render(request, 'index.html', {'companies': companies, 'employees': employees})

@login_required
def create_company(request):
	upload = CompanyCreate()
	if request.method == 'POST':
		upload = CompanyCreate(request.POST, request.FILES)
		if upload.is_valid():
			upload.save()
			return redirect('index')
		else:
			return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
	else:
		return render(request, 'add-company.html', {'upload_form':upload})

@login_required
def update_company(request, id):
        obj= get_object_or_404(Company, id=id)
        
        form = CompanyCreate(request.POST or None, instance= obj)
        context= {'form': form}

        if form.is_valid():
                obj= form.save(commit= False)

                obj.save()

                messages.success(request, "You successfully updated the company's details")
                return redirect('index')

                context= {'form': form}

                return render(request, 'edit-company.html', context)

        else:
                context= {'form': form,
                           'error': 'The form was not updated successfully. Please enter in a title and content'}
                return render(request,'edit-company.html' , context)


@login_required
def delete_company(request, id=None):

    company = get_object_or_404(Company, id=id)

    if request.method == "POST" and request.user.is_authenticated:
        company.delete()
        messages.success(request, "Company successfully deleted!")
        return redirect('index')
    
    context= {'company': company,
              }
    
    return render(request, 'company-delete.html', context)

@login_required
def create_employee(request):
	upload = EmployeeCreate()
	if request.method == 'POST':
		upload = EmployeeCreate(request.POST, request.FILES)
		if upload.is_valid():
			upload.save()
			return redirect('index')
		else:
			return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
	else:
		return render(request, 'add-employee.html', {'upload_form':upload})

@login_required
def update_employee(request, id):
        obj= get_object_or_404(Employee, id=id)
        
        form = EmployeeCreate(request.POST or None, instance= obj)
        context= {'form': form}

        if form.is_valid():
            obj= form.save(commit= False)

            obj.save()

            messages.success(request, "You successfully updated the employee's details")
            return redirect('index')

            context= {'form': form}

            return render(request, 'edit-employee.html', context)

        else:
            context= {'form': form,}
            return render(request,'edit-employee.html' , context)


@login_required
def delete_employee(request, id):
	id = int(id)
	try:
		employee_sel = Employee.objects.get(id = id)
	except Employee.DoesNotExist:
		return redirect('index')
	employee_sel.delete()
	return redirect('index')
