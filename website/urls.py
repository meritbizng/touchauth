from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^$', views.index, name='index'),
	url('company/create/', views.create_company, name = 'create_company'),
    url('company/edit/(?P<id>\d+)/$', views.update_company, name='update_company'),
    url('company/delete/(?P<id>\d+)/$', views.delete_company),

    url('employee/create', views.create_employee, name = 'create_employee'),
    url('employee/edit/(?P<id>\d+)/$', views.update_employee),
    url('employee/delete/(?P<id>\d+)/$', views.delete_employee),
	
]
